const apiCallFromRequest = require('./Request')
var params = require('./params.json');
var express = require("express");
var app = express();

app.listen(3000, () => {
    app.get("/bice", (req, res, next) => {
        if (req.url === "/bice") {
            apiCallFromRequest.callApi(function (response) {
                var workersInfo = response.policy.workers;
                var dental = response.policy.has_dental_care;
                var company_percentage  =parseFloat((response.policy.company_percentage/100)).toFixed(2);
                var employee_percentage =parseFloat((1 - company_percentage)).toFixed(2);
                var policy = 0;
                var retorno = new Object() ;
    
                for (var key in workersInfo) {
                    if (workersInfo[key]["age"] < params.max) {
                        if (workersInfo[key]["childs"] == 0) {
                            workersInfo[key]["copay"] = parseFloat(params.coberturas["vida"]["v1"]["value"] * employee_percentage).toFixed(4); 
                            policy += params.coberturas["vida"]["v1"]["value"];
                        } else if (workersInfo[key]["childs"] == 1) {
                            workersInfo[key]["copay"] = parseFloat(params.coberturas["vida"]["v2"]["value"] * employee_percentage).toFixed(4); 
                            policy += params.coberturas["vida"]["v2"]["value"];
                        }else{
                            policy += params.coberturas["vida"]["v3"]["value"];
                            workersInfo[key]["copay"] =parseFloat(params.coberturas["vida"]["v3"]["value"] * employee_percentage).toFixed(4);
                        }
                        
                    }else{
                        workersInfo[key]["copay"] = 0;
                    }

                    if(dental){
                        if (workersInfo[key]["childs"] == 0) {
                            workersInfo[key]["copay"] =+ parseFloat(params.coberturas["dental"]["d1"]["value"] * employee_percentage).toFixed(4); 
                            policy =+ params.coberturas["dental"]["d1"]["value"];
                            
                        } else if (workersInfo[key]["childs"] == 1) {
                            workersInfo[key]["copay"]=+ parseFloat(params.coberturas["dental"]["d2"]["value"] * employee_percentage).toFixed(4);
                            policy =+ params.coberturas["dental"]["d2"]["value"];
                        }else{
                            workersInfo[key]["copay"]=+ parseFloat(params.coberturas["dental"]["d3"]["value"] * employee_percentage).toFixed(4);
                            policy =+ params.coberturas["dental"]["d3"]["value"];
                        }
                        
                    }
                    
                }
                retorno["Message"] = "Policy App by Sebastian Perez to Bice Vida";
                retorno["policy"] = parseFloat(policy * company_percentage).toFixed(4);
                retorno["dental"] = dental;
                retorno["employee_percentage"] = employee_percentage;
                retorno["company_percentage"] = company_percentage;
                retorno["data"] = workersInfo;
                console.log(retorno);
                res.json(retorno);
                res.statusCode = 200;
                res.end();
            });
        } else {
            apiCallFromRequest.callApi(function (response) {
                res.statusCode = 404;
                res.end();
                
            });
    
        }

    });
});