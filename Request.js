const request = require('request');
 
_EXTERNAL_URL = 'https://dn8mlk7hdujby.cloudfront.net/interview/insurance/policy';

const callExternalApiUsingRequest = (callback) => {
    request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
    if (err) { 
        return callback(err);
     }
    return callback(body);
    });
}

module.exports.callApi = callExternalApiUsingRequest;